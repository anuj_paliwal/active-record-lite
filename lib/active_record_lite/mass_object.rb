require 'debugger'

class Class
  def my_attr_accessor(*args)
    args.each do |inst_var|
      self.send(:define_method, inst_var) do
        self.instance_variable_get("@#{inst_var.to_sym}")
      end

      self.send(:define_method, (inst_var.to_s + '=').to_sym) do |value|
        self.instance_variable_set( "@#{inst_var.to_s}".to_sym, value)
      end

    end
  end
end

class MassObject
  # takes a list of attributes.
  # creates getters and setters.
  # adds attributes to whitelist.
  def self.my_attr_accessible(*attributes)
    my_attr_accessor(*attributes)
    @attributes = *attributes
  end




  # returns list of attributes that have been whitelisted.
  def self.attributes
    @attributes
  end

  # takes an array of hashes.
  # returns array of objects.
  def self.parse_all(results)
    #results.each.map { |result| self.class.new }
  end

  # takes a hash of { attr_name => attr_val }.
  # checks the whitelist.
  # if the key (attr_name) is in the whitelist, the value (attr_val)
  # is assigned to the instance variable.
  def initialize(params = {})


    params.each do |name, value|
      raise "No good" unless self.class.attributes.include?(name.to_sym)
      self.instance_variable_set( "@#{name}".to_sym, value)
    end

  end
end